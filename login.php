<?php

  session_start();

  if (isset($_SESSION['user_id'])) {
    header('Location: /php-login');
  }
  require 'database.php';

  if (!empty($_POST['user']) && !empty($_POST['password'])) {
    $records = $conn->prepare('SELECT id, user, password FROM users WHERE user = :user');
    $records->bindParam(':user', $_POST['user']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $message = '';

    if (count($results) > 0 && password_verify($_POST['password'], $results['password'])) {
      $_SESSION['user_id'] = $results['id'];
      header("Location: index.html");
    } else {
      $message = 'Usuario o contraseña incorrecto';
    }
  }

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilo.css">
</head>

<body id="login1">
    <div class="container">
        <div class="row justify-content-center pt-5 mt-5 m-1">
            <div class="col-md-6 col-sm-8 col-xl-4 col-lg-5 formulario">
                <form action="login.php" method="POST">
                    <div class="form-group text-center pt-3">
                        <h1 class="text-light">Iniciar sesión</h1>
                    </div>
                    <?php if(!empty($message)): ?>
                    <p> <?= $message ?></p>
                    <?php endif; ?>
                    <div class="form-group mx-sm-4 pt-3">
                        <input name="user" type="text" class=" form-control" placeholder="Ingrese su Usuario">
                    </div>
                    <div class="form-group mx-sm-4 pt-3">
                        <input name="password" type="password" class="form-control" placeholder="Ingrese su contraseña">
                    </div>
                    <div class="form-group mx-sm-4 pt-2">
                        <input type="submit" class="btn btn-block ingresar " value=" ingresar">
                    </div>
                    <div class="form-group mx-sm-4 text-right">
                        <span class=""><a href="#" class="olvide btnRegistrar">Olvide mi contraseña?</a></span>
                    </div>

                    <br>
                    <span> <a href="signup.php" class="olvide1  btnRegistrar">Registrar</a></span>
                    <br>
                    <br>
                    <?php require 'partials/header.php' ?>
                </form>
            </div>
        </div>
    </div>

</body>

</html>