<?php

  require 'database.php';

  $message = '';

  
  if (!empty($_POST['fullname']) && !empty($_POST['email']) && !empty($_POST['user']) && !empty($_POST['password'])) {
    $sql = "INSERT INTO users (email, password,fullname,user) VALUES (:email, :password, :fullname, :user)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':email', $_POST['email']);
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':fullname', $_POST['fullname']);
    $stmt->bindParam(':user', $_POST['user']);
    

    if ($stmt->execute()) {
      $message = 'Usuario creado con exito';
    } else {
      $message = 'ha ocurrido un problema intentelo mas tarde';
    }
  }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SignUp</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilo.css">
</head>

<body id="registro">
    <div class="container">
        <div class="row justify-content-center pt-5 mt-5 m-1">
            <div class="col-md-6 col-sm-8 col-xl-4 col-lg-5 formulario">
                <form action="signup.php" method="POST">
                    <div class="form-group text-center pt-3">
                        <h1 class="text-light">Registrarse</h1>
                    </div>
                    <?php if(!empty($message)): ?>
                    <p> <?= $message ?></p>
                    <?php endif; ?>
                    <div class="form-group mx-sm-4 pt-3">
                        <input name="fullname" type="text" class="form-control" placeholder="Ingrese su nombre completo">
                    </div>
                    <div class="form-group mx-sm-4 pt-3">
                        <input name="email" type="text" class="form-control" placeholder="Ingrese su correo">
                    </div>
                    <div class="form-group mx-sm-4 pt-3">
                        <input name="user" type="text" class="form-control" placeholder="Ingrese su usuario">
                    </div>
                    <div class="form-group mx-sm-4 pt-3">
                        <input name="password" type="password" class="form-control" placeholder="Ingrese su contraseña">
                    </div>
                    <div class="form-group mx-sm-4 pt-2">
                        <input type="submit" class="btn btn-block ingresar" value=" Registrar">
                    </div>
                    <br>
                    <span> <a href="login.php" class="olvide1 btnRegistrar">Ya tengo cuenta</a></span>
                    <br>
                    <br>
                    <?php require 'partials/header.php' ?>
                </form>
            </div>
        </div>
    </div>

</body>

</html>